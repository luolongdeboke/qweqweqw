package com.atguigu.JDBC;

import com.atguigu.JDBC.Utils.JDBCUtils;
import org.junit.Test;

import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;


public class GetResultSet {



        public Object queryAll() throws Exception {
            Connection conn= JDBCUtils.getConnection();
            String sql = "select * from t_book";
            PreparedStatement ps = conn.prepareStatement(sql);
           ResultSet resultSet =(ResultSet)ps.executeQuery();

                   return resultSet;



    }

}
