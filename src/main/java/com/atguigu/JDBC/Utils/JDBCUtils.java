package com.atguigu.JDBC.Utils;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

public class JDBCUtils {
    /**
     * 获取连接
     * @return
     * @throws Exception
     */
    public static Connection getConnection () throws Exception {
        InputStream in = ClassLoader.getSystemClassLoader().getResourceAsStream("jdbc.properties");

        Properties pro=new Properties();
        pro.load(in);

        String user=pro.getProperty("user");
        String password=pro.getProperty("password");
        String url=pro.getProperty("url");
        String driverClass=pro.getProperty("driverClass");

        Class.forName(driverClass);
         Connection conn= DriverManager.getConnection(url,user,password);
         return conn;

    }





}
