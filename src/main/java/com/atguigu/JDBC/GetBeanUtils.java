package com.atguigu.JDBC;

import java.lang.reflect.Field;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;
import java.util.List;



public class GetBeanUtils {



    public static void main(String[] args) throws Exception {
        GetBeanUtils getBeanUtils=new GetBeanUtils();
        GetResultSet getResultSet=new GetResultSet();
        ResultSet   resultSet= (ResultSet) getResultSet.queryAll();

        getBeanUtils.setMetaData(resultSet, Bean.class);
    }

    private <T> List<T> setMetaData(ResultSet rs, Class<T> classname) throws Exception {
        List<T> List = new ArrayList<T>();
        T t = null;
        while (rs.next()) {
            t = classname.newInstance();
            ResultSetMetaData rsmd = rs.getMetaData();
            int count = rsmd.getColumnCount();
            for (int i = 1; i <= count; i++) {
                String name = rsmd.getColumnName(i);
                Field field = classname.getDeclaredField(name);
                field.setAccessible(true);
                field.set(t,rs.getObject(name));
            }
            List.add(t);
        }
        return List;
    }

}
